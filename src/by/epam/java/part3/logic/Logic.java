package by.epam.java.part3.logic;

import by.epam.java.part3.exceptions.NoSuchColorException;
import by.epam.java.part3.exceptions.NoSuchSortException;
import by.epam.java.part3.store.*;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;


public class Logic {

    /**
     * method that lets user enter a value of int type and throws error message
     * if anything else is entered, letting user enter a value again
     */

    public static int enterNumber() {

        Scanner scanner = new Scanner(System.in);

        while (!scanner.hasNextInt()) {

            System.out.println("Invalid number! Try again");
            System.out.print("> ");
            scanner.next();

        }

        return scanner.nextInt();

    }

    /**
     * method that checks given value of int type and throws error message
     * if value is less or equals to zero, letting user enter a value again
     */

    public static int noZerosAllowed(int i) {

        while (i <= 0) {

            System.out.println("You cannot buy less than 1 flower! Try again");
            System.out.print("> ");
            i = Logic.enterNumber();

        }

        return i;

    }

    /**
     * method that checks given value of int type and throws error message
     * if value is less than zero or more than the length of given array, letting user enter a value again
     */

    public static int validNumber(int i, String[] arr) {

        while (i < 0 || i > arr.length - 1) {

            System.out.println("Invalid number! Try again");
            System.out.print("> ");
            i = Logic.enterNumber();

        }

        return i;

    }

    /**
     * method that prints the contents of given array of String type
     * given String makes the method more flexible
     */

    public static void selectFromArray(String[] arr, String msg) {

        System.out.print("Please enter a number of particular flower's " + msg);

        for (int i = 0; i < arr.length; i++) {

            if (i == arr.length - 1) {
                System.out.print("and " + i + " for " + arr[i] + "\n");
                return;
            }

            System.out.print(i + " for " + arr[i] + ", ");

        }

    }

    /**
     * method that returns the array of colors for a certain flower,
     * depending on the sort selected by the user (given value of int type)
     */

    public static String[] getValidColors(int selectedSort) {

        switch (selectedSort) {

            case 0:
                return Carnation.getCarnationColors();
            case 1:
                return Chamomile.getChamomileColors();
            case 2:
                return Cornflower.getCornflowerColors();
            case 3:
                return Daffodil.getDaffodilColors();
            case 4:
                return Lilac.getLilacColors();
            case 5:
                return Tulip.getTulipColors();
            case 6:
                return Rose.getRoseColors();
            case 7:
                return Viola.getViolaColors();
            default:
                throw new NoSuchElementException("This sort of flower does not exist!");
        }
    }

    /**
     * method that creates an object of a certain class
     * depending on the sort selected by the user (given value of int type)
     * with specified quantity (given value of int type) and color (given value of String type)
     * after creation, object is being put into an ArrayList of by.epam.java.part3.store.Flower type (which is given and passed as a parameter)
     */

    public static List<Flower> putFlowerIntoArray(List<Flower> flowers, int selectedSort, int qty, String flowerColor) {

        switch (selectedSort) {

            case 0:
                Carnation carnation = new Carnation(qty, flowerColor);
                flowers.add(carnation);
                return flowers;
            case 1:
                Chamomile chamomile = new Chamomile(qty, flowerColor);
                flowers.add(chamomile);
                return flowers;
            case 2:
                Cornflower cornflower = new Cornflower(qty, flowerColor);
                flowers.add(cornflower);
                return flowers;
            case 3:
                Daffodil daffodil = new Daffodil(qty, flowerColor);
                flowers.add(daffodil);
                return flowers;
            case 4:
                Lilac lilac = new Lilac(qty, flowerColor);
                flowers.add(lilac);
                return flowers;
            case 5:
                Tulip tulip = new Tulip(qty, flowerColor);
                flowers.add(tulip);
                return flowers;
            case 6:
                Rose rose = new Rose(qty, flowerColor);
                flowers.add(rose);
                return flowers;
            case 7:
                Viola viola = new Viola(qty, flowerColor);
                flowers.add(viola);
                return flowers;
            default:
                return flowers;

        }

    }

    /**
     * method that checks the last object in ArrayList of type Flower
     * (which is basically the content of given object of Bouquet class)
     * against each object in that ArrayList. If there's a match in sort AND color:
     * - quantities of the last object and the matching object are being added
     * - new object of matching class is being created
     * - matching object is being removed from ArrayList
     * - new object is being added to ArrayList
     * - the object which launched comparison in the first place is being removed from ArrayList
     */

    public static Bouquet checkDuplicates(Bouquet bouquet, int i) {

        if (bouquet.getFlowers().get(i).getColor().equalsIgnoreCase(bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getColor()) && bouquet.getFlowers().get(i).getSort().equalsIgnoreCase(bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getSort()) && bouquet.getFlowers().size() > 1) {

            int qty;

            if (Carnation.getSORT().equalsIgnoreCase(bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getSort())) {
                qty = bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getQuantity() + bouquet.getFlowers().get(i).getQuantity();
                Carnation carnation = new Carnation(qty, bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getColor());
                bouquet.getFlowers().remove(i);
                bouquet.getFlowers().add(carnation);
                bouquet.getFlowers().remove(bouquet.getFlowers().size() - 2);
            } else if (Chamomile.getSORT().equalsIgnoreCase(bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getSort())) {
                qty = bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getQuantity() + bouquet.getFlowers().get(i).getQuantity();
                Chamomile chamomile = new Chamomile(qty, bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getColor());
                bouquet.getFlowers().remove(i);
                bouquet.getFlowers().add(chamomile);
                bouquet.getFlowers().remove(bouquet.getFlowers().size() - 2);
            } else if (Cornflower.getSORT().equalsIgnoreCase(bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getSort())) {
                qty = bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getQuantity() + bouquet.getFlowers().get(i).getQuantity();
                Cornflower cornflower = new Cornflower(qty, bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getColor());
                bouquet.getFlowers().remove(i);
                bouquet.getFlowers().add(cornflower);
                bouquet.getFlowers().remove(bouquet.getFlowers().size() - 2);
            } else if (Daffodil.getSORT().equalsIgnoreCase(bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getSort())) {
                qty = bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getQuantity() + bouquet.getFlowers().get(i).getQuantity();
                Daffodil daffodil = new Daffodil(qty, bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getColor());
                bouquet.getFlowers().remove(i);
                bouquet.getFlowers().add(daffodil);
                bouquet.getFlowers().remove(bouquet.getFlowers().size() - 2);
            } else if (Lilac.getSORT().equalsIgnoreCase(bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getSort())) {
                qty = bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getQuantity() + bouquet.getFlowers().get(i).getQuantity();
                Lilac lilac = new Lilac(qty, bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getColor());
                bouquet.getFlowers().remove(i);
                bouquet.getFlowers().add(lilac);
                bouquet.getFlowers().remove(bouquet.getFlowers().size() - 2);
            } else if (Rose.getSORT().equalsIgnoreCase(bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getSort())) {
                qty = bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getQuantity() + bouquet.getFlowers().get(i).getQuantity();
                Rose rose = new Rose(qty, bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getColor());
                bouquet.getFlowers().remove(i);
                bouquet.getFlowers().add(rose);
                bouquet.getFlowers().remove(bouquet.getFlowers().size() - 2);
            } else if (Tulip.getSORT().equalsIgnoreCase(bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getSort())) {
                qty = bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getQuantity() + bouquet.getFlowers().get(i).getQuantity();
                Tulip tulip = new Tulip(qty, bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getColor());
                bouquet.getFlowers().remove(i);
                bouquet.getFlowers().add(tulip);
                bouquet.getFlowers().remove(bouquet.getFlowers().size() - 2);
            } else if (Viola.getSORT().equalsIgnoreCase(bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getSort())) {
                qty = bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getQuantity() + bouquet.getFlowers().get(i).getQuantity();
                Viola viola = new Viola(qty, bouquet.getFlowers().get(bouquet.getFlowers().size() - 1).getColor());
                bouquet.getFlowers().remove(i);
                bouquet.getFlowers().add(viola);
                bouquet.getFlowers().remove(bouquet.getFlowers().size() - 2);
            }

        }

        return bouquet;

    }

    /**
     * method that counts the total price of the object inside ArrayList of by.epam.java.part3.store.Flower type
     * by multiplying its quantity and price for piece
     * (given are such ArrayList and a value of int type which represents an index in ArrayList)
     */

    public static double getTotalWithTwoDecimals(List<Flower> flowers, int n) {

        double total = flowers.get(n).getTotalPrice();
        String totalCost = String.format("%.2f", total);
        totalCost = totalCost.replaceAll(",", ".");
        return Double.valueOf(totalCost);

    }

    /**
     * method that prints user's current selection
     * including specific object's quantity, color and sort, as well as total object's price
     * given are object of by.epam.java.part3.store.Bouquet class and value of double type
     */

    public static void printCurrentSelection(Bouquet bouquet, double total) {

        System.out.println("---------------------------------------");
        System.out.println("You have selected");

        if (bouquet.getQuantities(bouquet.getFlowers().size() - 1) == 1) {
            System.out.println(bouquet.getQuantities(bouquet.getFlowers().size() - 1) + " " + bouquet.getColors(bouquet.getFlowers().size() - 1) + " " + bouquet.getSorts(bouquet.getFlowers().size() - 1));
        } else {
            System.out.println(bouquet.getQuantities(bouquet.getFlowers().size() - 1) + " " + bouquet.getColors(bouquet.getFlowers().size() - 1) + " " + bouquet.getSorts(bouquet.getFlowers().size() - 1) + "s");
        }

        System.out.println("---------------------------------------");
        System.out.println("The price for your selection is " + String.format("%.2f", total) + " dollars\n");


    }

    /**
     * method that prints user's final selection
     * including each object's quantity, color and sort
     * given is object of by.epam.java.part3.store.Bouquet class
     */

    public static void printBouquet(Bouquet bouquet) {

        for (Flower element : bouquet.getFlowers()) {

            if (element.getQuantity() == 1) {
                System.out.println(element.getQuantity() + " " + element.getColor() + " " + element.getSort());
            } else {
                System.out.println(element.getQuantity() + " " + element.getColor() + " " + element.getSort() + "s");
            }
        }
    }

    /**
     * method that prints user's check including each object's quantity,
     * color and sort, as well as total price for a bouquet
     * given are object of by.epam.java.part3.store.Bouquet class and value of double type
     */

    public static void printCheck(double sum, Bouquet bouquet) {

        System.out.println("---------------------------------------");
        System.out.println("Your bouquet has");

        Logic.printBouquet(bouquet);

        System.out.println("---------------------------------------");
        System.out.println("The price for the bouquet is " + String.format("%.2f", sum) + " dollars");
        System.out.println("---------------------------------------");
        System.out.println("Thanks for shopping at MadFlowersCo.");

    }

    /**
     * method that takes elements of the by.epam.java.part3.store.Bouquet object and puts them into
     * ArrayList as Strings (each String has flower's Quantity, Color and Sort)
     * then ArrayList of Strings is turned into one String that has all items from ArrayList
     * done in such way so that by.epam.java.part3.store.Bouquet elements could be written to file in readable form
     */

    public static String printCheckToFile(double sum, Bouquet bouquet) {

        List<String> list = new ArrayList<>();

        for (Flower element : bouquet.getFlowers()) {
            if (element.getQuantity() == 1) {
                list.add(String.valueOf(element.getQuantity()).concat(" ").concat(element.getColor()).concat(" ").concat(element.getSort()));
            } else {
                list.add(String.valueOf(element.getQuantity()).concat(" ").concat(element.getColor()).concat(" ").concat(element.getSort()).concat("s"));
            }
        }

        String str5 = list.get(0);

        for (int i = 1; i < list.size(); i++) {
            str5 = str5.concat("\n").concat(list.get(i));
        }

        return str5;

    }

    /**
     * method that checks if given color (String object) matches any color from given array
     * of Strings otherwise throws NoSuchElementException
     * done in such way because each flower has different set of colors
     */

    public static String colorExists(String color, String[] arr) throws NoSuchColorException {

        boolean b = false;

        for (int i = 0; i < arr.length; i++) {

            if (color.equalsIgnoreCase(arr[i])) {
                color = arr[i];
                b = true;
            }
        }
        if (b == false) {
            throw new NoSuchColorException("No such color available for selected sort!");
        }
        return color;
    }

    /**
     * method that checks if given sort (String object) matches any sort from given array
     * of Strings. Returns the number of the matching sort from given array
     * otherwise throws NoSuchElementException
     */

    public static int sortExists(String sort, String[] arr) throws NoSuchSortException {

        int b = arr.length + 1;

        for (int i = 0; i < arr.length; i++) {

            if (arr[i].equalsIgnoreCase(sort)) {
                b = i;
            }
        }
        if (b == arr.length + 1) {
            throw new NoSuchSortException("There's no such sort in our store!");
        } else {
            return b;
        }
    }

    /**
     * method that takes totalPrice value from each object in given List and sums them
     * returns double value of the sum
     */

    public static double countTotalBouquetCost(List<Flower> flowers) {

        double sum = 0;

        for (int i = 0; i < flowers.size(); i++) {
            sum += Logic.getTotalWithTwoDecimals(flowers, i);
        }

        return sum;
    }

}