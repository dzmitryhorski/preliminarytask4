package by.epam.java.part3.store;

public class Lilac extends Flower {

    private static final double PRICE = 0.60;
    private static final String SORT = "Lilac";
    private static final String[] lilacColors = {"White", "Red", "Pink", "Blue", "Violet"};

    public Lilac(int qty, String color) {
        super(qty, color, Lilac.SORT, Lilac.countTotalPrice(qty));
    }

    public static String[] getLilacColors() {
        return lilacColors;
    }

    public static double getPRICE() {
        return PRICE;
    }

    public static double countTotalPrice(int qty) {
        return getPRICE() * qty;
    }

    public static String getSORT() {
        return SORT;
    }
}
