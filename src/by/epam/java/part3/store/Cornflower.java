package by.epam.java.part3.store;

public class Cornflower extends Flower {

    private static final double PRICE = 0.50;
    private static final String SORT = "Cornflower";
    private static final String[] cornflowerColors = {"White", "Red", "Pink", "Yellow", "Blue", "Violet"};

    public Cornflower(int qty, String color) {

        super(qty, color, Cornflower.SORT, Cornflower.countTotalPrice(qty));

    }

    public static String[] getCornflowerColors() {
        return cornflowerColors;
    }

    public static double getPRICE() {
        return PRICE;
    }

    public static double countTotalPrice(int qty) {
        return getPRICE() * qty;
    }

    public static String getSORT() {
        return SORT;
    }
}
