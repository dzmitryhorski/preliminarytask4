package by.epam.java.part3.store;

public class Chamomile extends Flower {

    private static final double PRICE = 0.40;
    private static final String SORT = "Chamomile";
    private static final String[] chamomileColors = {"White", "Red", "Pink", "Yellow"};

    public Chamomile(int qty, String color) {
        super(qty, color, Chamomile.SORT, Chamomile.countTotalPrice(qty));
    }

    public static String[] getChamomileColors() {
        return chamomileColors;
    }

    public static double getPRICE() {
        return PRICE;
    }

    public static double countTotalPrice(int qty) {
        return getPRICE() * qty;
    }

    public static String getSORT() {
        return SORT;
    }
}
