package by.epam.java.part3.store;

public class Rose extends Flower {

    private static final double PRICE = 0.55;
    private static final String SORT = "Rose";
    private static final String[] roseColors = {"White", "Black", "Red", "Pink", "Orange", "Yellow", "Green", "Blue", "Violet"};

    public Rose(int qty, String color) {
        super(qty, color, Rose.SORT, Rose.countTotalPrice(qty));
    }

    public static String[] getRoseColors() {
        return roseColors;
    }

    public static double getPRICE() {
        return PRICE;
    }

    public static double countTotalPrice(int qty) {
        return getPRICE() * qty;
    }

    public static String getSORT() {
        return SORT;
    }
}
