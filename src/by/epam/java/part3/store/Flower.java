package by.epam.java.part3.store;

public class Flower {

    private int quantity;
    private String color;
    private double totalPrice;
    private String sort;

    public Flower(int quantity, String color, String sort, double totalPrice) {

        this.quantity = quantity;
        this.color = color;
        this.sort = sort;
        this.totalPrice = totalPrice;

    }

    public Flower() {
        this.quantity = 0;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getColor() {
        return color;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

}


