package by.epam.java.part3.store;

public class Daffodil extends Flower {

    private static final double PRICE = 0.75;
    private static final String SORT = "Daffodil";
    private static final String[] daffodilColors = {"White", "Orange", "Yellow"};

    public Daffodil(int qty, String color) {
        super(qty, color, Daffodil.SORT, Daffodil.countTotalPrice(qty));
    }

    public static String[] getDaffodilColors() {
        return daffodilColors;
    }

    public static double getPRICE() {
        return PRICE;
    }

    public static double countTotalPrice(int qty) {
        return getPRICE() * qty;
    }

    public static String getSORT() {
        return SORT;
    }
}
