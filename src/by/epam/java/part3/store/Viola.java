package by.epam.java.part3.store;

public class Viola extends Flower {

    private static final double PRICE = 0.35;
    private static final String SORT = "Viola";
    private static final String[] violaColors = {"White", "Red", "Pink", "Green", "Blue", "Violet"};

    public Viola(int qty, String color) {
        super(qty, color, Viola.SORT, Viola.countTotalPrice(qty));
    }

    public static String[] getViolaColors() {
        return violaColors;
    }

    public static double getPRICE() {
        return PRICE;
    }

    public static double countTotalPrice(int qty) {
        return getPRICE() * qty;
    }

    public static String getSORT() {
        return SORT;
    }
}
