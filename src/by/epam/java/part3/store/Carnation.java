package by.epam.java.part3.store;

public class Carnation extends Flower {

    private static final double PRICE = 0.50;
    private static final String SORT = "Carnation";
    private static final String[] carnationColors = {"White", "Red", "Pink", "Yellow", "Violet"};

    public Carnation(int qty, String color) {
        super(qty, color, Carnation.SORT, Carnation.countTotalPrice(qty));
    }

    public static String[] getCarnationColors() {
        return carnationColors;
    }

    public static double getPRICE() {
        return PRICE;
    }

    public static double countTotalPrice(int qty) {

        return getPRICE() * qty;

    }

    public static String getSORT() {
        return SORT;
    }
}
