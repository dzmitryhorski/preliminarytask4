package by.epam.java.part3.store;

public class Tulip extends Flower {

    private static final double PRICE = 0.25;
    private static final String SORT = "Tulip";
    private static final String[] tulipColors = {"White", "Black", "Red", "Pink", "Orange", "Yellow", "Blue", "Violet"};

    public Tulip(int qty, String color) {
        super(qty, color, Tulip.SORT, Tulip.countTotalPrice(qty));
    }

    public static String[] getTulipColors() {
        return tulipColors;
    }

    public static double getPRICE() {
        return PRICE;
    }

    public static double countTotalPrice(int qty) {
        return getPRICE() * qty;
    }

    public static String getSORT() {
        return SORT;
    }
}
