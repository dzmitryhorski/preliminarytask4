package by.epam.java.part3.store;

import java.util.List;

public class Bouquet {

    protected List<Flower> flowers;


    public Bouquet(List<Flower> flowers) {

        this.flowers = flowers;

    }

    public List<Flower> getFlowers() {

        return flowers;

    }

    public int getQuantities(int i) {

        return flowers.get(i).getQuantity();

    }

    public String getSorts(int i) {

        return flowers.get(i).getSort();

    }

    public String getColors(int i) {

        return flowers.get(i).getColor();

    }

}
