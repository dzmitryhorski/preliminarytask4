package by.epam.java.part3.exceptions;

public class NoSuchSortException extends Exception {
    public NoSuchSortException(String errorMessage) {
        super(errorMessage);
    }
}