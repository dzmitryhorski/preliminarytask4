package by.epam.java.part3.exceptions;

public class NoSuchColorException extends Exception {
    public NoSuchColorException(String errorMessage) {
        super(errorMessage);
    }
}
