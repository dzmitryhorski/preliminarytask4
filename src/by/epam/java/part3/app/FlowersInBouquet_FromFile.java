package by.epam.java.part3.app;

import java.io.*;
import java.util.*;
import by.epam.java.part3.exceptions.*;

import by.epam.java.part3.logic.Logic;
import by.epam.java.part3.store.*;

public class FlowersInBouquet_FromFile {

    public static void main(String[] args) {

        int qty = 0;
        String color = "";
        String sort = "";

        List<String> order = new ArrayList<>();

        String[] sorts = {Carnation.getSORT(), Chamomile.getSORT(), Cornflower.getSORT(), Daffodil.getSORT(), Lilac.getSORT(), Tulip.getSORT(), Rose.getSORT(), Viola.getSORT()};
        List<Flower> flowers = new ArrayList<>();
        Bouquet bouquet = null;

        BufferedReader br = null;
        BufferedWriter bw = null;

        try {
            try {
                File orderFile = new File("./orders/Order.txt");
                File checkFile = new File("./checks/Check.txt");

                br = new BufferedReader(new FileReader(orderFile));
                bw = new BufferedWriter(new FileWriter(checkFile));
                String text;
                while ((text = br.readLine()) != null) {
                    order.add(text);
                }

                StringTokenizer st;

                for (String anOrder : order) {

                    st = new StringTokenizer(anOrder, " ");

                        while (st.hasMoreTokens()) {
                            qty = Integer.parseInt(st.nextToken());
                            color = st.nextToken();
                            sort = st.nextToken();
                    }

                    char lastCharacter = sort.charAt(sort.length() - 1);

                    if (lastCharacter == 's') {
                        sort = sort.substring(0, sort.length() - 1);
                    }

                    int selectedSort = Logic.sortExists(sort, sorts);
                    color = Logic.colorExists(color, Logic.getValidColors(selectedSort));

                    flowers = Logic.putFlowerIntoArray(flowers, selectedSort, qty, color);
                    bouquet = new Bouquet(flowers);

                    for (int i = 0; i < bouquet.getFlowers().size() - 1; i++) {
                        Logic.checkDuplicates(bouquet, i);
                    }
                }

                double sum = Logic.countTotalBouquetCost(flowers);
                String str1 = "---------------------------------------";
                String str2 = "Your bouquet has";
                String str3 = "The price for the bouquet is ".concat(String.format("%.2f", sum)).concat(" dollars");
                String str4 = "Thanks for shopping at MadFlowersCo.";
                String str5 = Logic.printCheckToFile(sum, bouquet);
                bw.write(str1 + "\n" + str2 + "\n" + str1 + "\n" + str5 + "\n" + str1 + "\n" + str3 + "\n" + str1 + "\n" + str4);
                bw.flush();

            } catch (NoSuchSortException | NoSuchColorException | ArrayIndexOutOfBoundsException | FileNotFoundException | NullPointerException | NumberFormatException exc) {
                assert bw != null;
                bw.write("Sorry, the order wasn't executed.\n");
                bw.write(exc.getMessage());
            } finally {
                if (br != null)
                    br.close();
                if (bw != null)
                    bw.close();
            }
        } catch (IOException ex) {
            try {
                assert bw != null;
                bw.write("Sorry, the order wasn't executed.\n");
                bw.write(ex.getMessage());
            } catch (IOException ignore) {
            }
        }

    }
}