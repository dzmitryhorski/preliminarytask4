package by.epam.java.part3.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import by.epam.java.part3.logic.Logic;
import by.epam.java.part3.store.*;

public class FlowersInBouquet_FromConsole {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        boolean b = true;

        double sum = 0;

        String[] sorts = {Carnation.getSORT(), Chamomile.getSORT(), Cornflower.getSORT(), Daffodil.getSORT(), Lilac.getSORT(), Tulip.getSORT(), Rose.getSORT(), Viola.getSORT()};

        List<Flower> flowers = new ArrayList<>();

        while (b) {

            Logic.selectFromArray(sorts, "sort: ");
            System.out.print("> ");
            int selectedSort = Logic.validNumber(Logic.enterNumber(), sorts);
            String[] validColors = Logic.getValidColors(selectedSort);

            Logic.selectFromArray(validColors, "color: ");
            System.out.print("> ");
            String flowerColor = validColors[Logic.validNumber(Logic.enterNumber(), validColors)];

            System.out.println("Please enter the quantity of items: ");
            System.out.print("> ");
            int qty = Logic.noZerosAllowed(Logic.enterNumber());

            flowers = Logic.putFlowerIntoArray(flowers, selectedSort, qty, flowerColor);

            double total = Logic.getTotalWithTwoDecimals(flowers, flowers.size() - 1);

            Bouquet bouquet = new Bouquet(flowers);

            Logic.printCurrentSelection(bouquet, total);

            for (int i = 0; i < bouquet.getFlowers().size() - 1; i++) {
                Logic.checkDuplicates(bouquet, i);
            }

            System.out.print("Do you want to continue shopping? Y / N: ");

            if (scanner.next().equalsIgnoreCase("y")) {

                sum += total;
                System.out.println("---------------------------------------");
                System.out.println("Current total is " + String.format("%.2f", sum) + " dollars");
                System.out.println("---------------------------------------");

            } else {

                sum += total;
                Logic.printCheck(sum, bouquet);
                b = false;

            }

        }

    }

}